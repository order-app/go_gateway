package client

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/labstack/gommon/log"
	"io"
	"net/http"
	"order_customer_gateway/go_gateway/dto"
	"order_customer_gateway/pkg/client"
	"order_customer_gateway/pkg/helper/errors"
)

type CustomerClient struct {
	client *client.Client
}

func NewCustomerClient(url string) *CustomerClient {
	return &CustomerClient{client: client.NewClient(url)}
}

func (c *CustomerClient) GetCustomers(url string) dto.CustomersResponseType {
	req, _ := http.NewRequestWithContext(context.Background(), "GET", fmt.Sprintf("%s%s", c.client.BaseURI, url), nil)
	res := c.client.SendRequest(req)
	defer res.Close()
	var customersResponse dto.CustomersResponseType
	if err := json.NewDecoder(res).Decode(&customersResponse); err != nil {
		log.Errorf("Send request response decode error : %s", err)
		errors.Panic(errors.ClientError)
	}
	return customersResponse
}

func (c *CustomerClient) GetCustomer(id string) dto.CustomerResponseType {
	req, _ := http.NewRequestWithContext(context.Background(), "GET", fmt.Sprintf("%s/%s", c.client.BaseURI, id), nil)
	res := c.client.SendRequest(req)
	defer res.Close()
	var customerResponse dto.CustomerResponseType
	if err := json.NewDecoder(res).Decode(&customerResponse); err != nil {
		log.Errorf("Send request response decode error : %s", err)
		errors.Panic(errors.ClientError)
	}
	return customerResponse
}

func (c *CustomerClient) DeleteOneCustomer(id string) {
	req, _ := http.NewRequestWithContext(context.Background(), "DELETE", fmt.Sprintf("%s/%s", c.client.BaseURI, id), nil)
	c.client.SendRequest(req)
}

func (c *CustomerClient) CreateCustomer(createBody io.ReadCloser) dto.IdResponseType {
	req, _ := http.NewRequestWithContext(context.Background(), "POST", c.client.BaseURI, createBody)
	res := c.client.SendRequest(req)
	var customerId dto.IdResponseType
	if err := json.NewDecoder(res).Decode(&customerId); err != nil {
		log.Errorf("Send request response decode error : %s", err)
		errors.Panic(errors.ClientError)
	}
	return customerId
}

func (c *CustomerClient) UpdateCustomer(id string, updateBody io.ReadCloser) {
	req, _ := http.NewRequestWithContext(context.Background(), "PUT", fmt.Sprintf("%s/%s", c.client.BaseURI, id), updateBody)
	c.client.SendRequest(req)
}
