package client

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/labstack/gommon/log"
	"io"
	"net/http"
	"order_customer_gateway/go_gateway/dto"
	"order_customer_gateway/pkg/client"
	"order_customer_gateway/pkg/helper/errors"
)

type OrderClient struct {
	client *client.Client
}

func NewOrderClient(url string) *OrderClient {
	return &OrderClient{client: client.NewClient(url)}
}

func (c *OrderClient) GetOrders(url string) dto.FindManyResponseType {
	req, _ := http.NewRequestWithContext(context.Background(), "GET", fmt.Sprintf("%s%s", c.client.BaseURI, url), nil)
	res := c.client.SendRequest(req)
	defer res.Close()
	var ordersResponse dto.FindManyResponseType
	if err := json.NewDecoder(res).Decode(&ordersResponse); err != nil {
		log.Errorf("Send request response decode error : %s", err)
		errors.Panic(errors.ClientError)
	}
	return ordersResponse
}

func (c *OrderClient) GetOrder(id string) dto.OrderResponseType {
	req, _ := http.NewRequestWithContext(context.Background(), "GET", fmt.Sprintf("%s/%s", c.client.BaseURI, id), nil)
	res := c.client.SendRequest(req)
	defer res.Close()
	var orderRes dto.OrderResponseType
	if err := json.NewDecoder(res).Decode(&orderRes); err != nil {
		log.Errorf("Send request response decode error : %s", err)
		errors.Panic(errors.ClientError)
	}
	return orderRes
}

func (c *OrderClient) DeleteOneOrder(id string) {
	req, _ := http.NewRequestWithContext(context.Background(), "DELETE", fmt.Sprintf("%s/%s", c.client.BaseURI, id), nil)
	c.client.SendRequest(req)
}

func (c *OrderClient) UpdateOrder(id string, updateBody io.ReadCloser) {
	req, _ := http.NewRequestWithContext(context.Background(), "PUT", fmt.Sprintf("%s/%s", c.client.BaseURI, id), updateBody)
	c.client.SendRequest(req)
}

func (c *OrderClient) CreateOrder(createBody io.ReadCloser) dto.IdResponseType {
	req, _ := http.NewRequestWithContext(context.Background(), "POST", c.client.BaseURI, createBody)
	res := c.client.SendRequest(req)
	var orderId dto.IdResponseType
	if err := json.NewDecoder(res).Decode(&orderId); err != nil {
		log.Errorf("Send request response decode error : %s", err)
		errors.Panic(errors.ClientError)
	}
	return orderId
}

func (c *OrderClient) UpdateOrderStatus(id string, updateBody io.ReadCloser) {
	req, _ := http.NewRequestWithContext(context.Background(), "PUT", fmt.Sprintf("%s/status/%s", c.client.BaseURI, id), updateBody)
	c.client.SendRequest(req)
}
