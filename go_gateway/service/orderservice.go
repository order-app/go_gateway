package service

import (
	"io"
	"net/url"
	"order_customer_gateway/go_gateway/client"
	"order_customer_gateway/go_gateway/dto"
)

type OrderService struct {
	client *client.OrderClient
}

func NewOrderService(client *client.OrderClient) *OrderService {
	return &OrderService{client: client}
}

func (s *OrderService) FindOrders(params url.Values) dto.FindManyResponseType {
	var urlVal string
	if params != nil {
		for key, value := range params {
			if urlVal == "" {
				urlVal = urlVal + "?" + key + "=" + value[0]
			} else {
				urlVal = urlVal + "&" + key + "=" + value[0]
			}
		}
	}
	return s.client.GetOrders(urlVal)
}

func (s *OrderService) FindOrder(id string) dto.OrderResponseType {
	return s.client.GetOrder(id)
}

func (s *OrderService) DeleteOrder(id string) {
	s.client.DeleteOneOrder(id)
}

func (s *OrderService) CreateOrder(body io.ReadCloser) dto.IdResponseType {
	return s.client.CreateOrder(body)
}

func (s *OrderService) UpdateOrder(id string, body io.ReadCloser) {
	s.client.UpdateOrder(id, body)
}

func (s *OrderService) UpdateOrderStatus(id string, body io.ReadCloser) {
	s.client.UpdateOrderStatus(id, body)
}
