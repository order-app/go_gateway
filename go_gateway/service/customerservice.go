package service

import (
	"io"
	"net/url"
	"order_customer_gateway/go_gateway/client"
	"order_customer_gateway/go_gateway/dto"
)

type CustomerService struct {
	client *client.CustomerClient
}

func NewCustomerService(client *client.CustomerClient) *CustomerService {
	return &CustomerService{client: client}
}

func (s *CustomerService) FindCustomers(params url.Values) dto.CustomersResponseType {
	var urlVal string
	if params != nil {
		for key, value := range params {
			if urlVal == "" {
				urlVal = urlVal + "?" + key + "=" + value[0]
			} else {
				urlVal = urlVal + "&" + key + "=" + value[0]
			}
		}
	}
	return s.client.GetCustomers(urlVal)
}

func (s *CustomerService) FindCustomer(id string) dto.CustomerResponseType {
	return s.client.GetCustomer(id)
}

func (s *CustomerService) DeleteCustomer(id string) {
	s.client.DeleteOneCustomer(id)
}

func (s *CustomerService) CreateCustomer(body io.ReadCloser) dto.IdResponseType {
	return s.client.CreateCustomer(body)
}

func (s *CustomerService) UpdateCustomer(id string, body io.ReadCloser) {
	s.client.UpdateCustomer(id, body)
}
