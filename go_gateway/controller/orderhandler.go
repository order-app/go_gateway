package controller

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"order_customer_gateway/go_gateway/dto"
	"order_customer_gateway/go_gateway/service"
)

type OrderHandler struct {
	service *service.OrderService
}

func NewOrderHandler(orderService *service.OrderService) *OrderHandler {
	return &OrderHandler{service: orderService}
}

// GetOrders
//@Summary      List orders
// @Description  get orders
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        sort query string false "sort"
// @Param        page query int false "page"
// @Param		 customerId query string false "customerId"
// @Param		 quantity query int false "quantity"
// @Param		 price query float64 false "price"
// @Param		 product.name query string false "product name"
// @Param		 product.imageUrl query string false "product.imageUrl"
// @Param		 address.addressLine query string false "address line"
// @Param		 address.city query string false "city"
// @Param 		 address.country query string false "country"
// @Param		 address.cityCode query int false "city code"
// @Success      200  {object}  dto.SuccessResponseType
// @Failure		 404 {object} 	dto.ErrorType
// @Router       /api/order 	[get]
func (h *OrderHandler) GetOrders(c echo.Context) error {
	orders := h.service.FindOrders(c.QueryParams())
	return c.JSON(http.StatusOK, dto.NewSuccessResponse(orders))
}

// GetByIdOrder
//@Summary       Show an order
// @Description  get order by ID
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Order ID"
// @Success      200  {object}  dto.SuccessResponseType
// @Failure		 404 {object} 	dto.ErrorType
// @Failure		 400 {object} 	dto.ErrorType
// @Router       /api/order/{id} [get]
func (h *OrderHandler) GetByIdOrder(c echo.Context) error {
	order := h.service.FindOrder(c.Param("id"))
	return c.JSON(http.StatusOK, dto.NewSuccessResponse(order))
}

// DeleteOrder
//@Summary       Delete an order
// @Description  delete order by ID
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Order ID"
// @Success      200
//@Failure      400  {object}  dto.ErrorType
// @Failure		 404 {object} 	dto.ErrorType
// @Router       /api/order/{id} [delete]
func (h *OrderHandler) DeleteOrder(c echo.Context) error {
	h.service.DeleteOrder(c.Param("id"))
	return c.JSON(http.StatusNoContent, nil)
}

// CreateOrder
//@Summary       Add an order
// @Description  create order
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param 	dto.RequestOrderType body dto.RequestOrderType true "For create an Order"
//@Success      200  {object}  dto.IdResponseType
// @Failure		 400 {object} 	dto.ErrorType
// @Failure		 500 {object} 	dto.ErrorType
// @Router       /api/order [post]
func (h *OrderHandler) CreateOrder(c echo.Context) error {
	idResponse := h.service.CreateOrder(c.Request().Body)
	return c.JSON(http.StatusCreated, idResponse)
}

// UpdateOrder
//@Summary       Update an order
// @Description  update order
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Order ID"
// @Param 		dto.RequestOrderType body dto.RequestOrderType true "For update an Order"
// @Success      200  {object}  dto.QuickResponseType
// @Failure		 404 {object} 	dto.ErrorType
// @Failure		 400 {object} 	dto.ErrorType
// @Failure		 500 {object} 	dto.ErrorType
// @Router       /api/order/{id} [put]
func (h *OrderHandler) UpdateOrder(c echo.Context) error {
	h.service.UpdateOrder(c.Param("id"), c.Request().Body)
	return c.JSON(http.StatusOK, dto.QuickResponseType{Result: true})
}

// UpdateOrderStatus
//@Summary       Update an order status
// @Description  update order status
// @Tags         orders
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Order ID"
// @Param 		dto.ChangeStatusReq body dto.ChangeStatusReq true "For update an Order Status"
// @Success      200  {object}  dto.QuickResponseType
// @Failure		 404 {object} 	dto.ErrorType
// @Failure		 400 {object} 	dto.ErrorType
// @Router       /api/order/status/{id} [put]
func (h *OrderHandler) UpdateOrderStatus(c echo.Context) error {
	h.service.UpdateOrderStatus(c.Param("id"), c.Request().Body)
	return c.JSON(http.StatusOK, dto.QuickResponseType{Result: true})
}
