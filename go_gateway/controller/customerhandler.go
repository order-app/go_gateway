package controller

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"order_customer_gateway/go_gateway/dto"
	"order_customer_gateway/go_gateway/service"
)

type CustomerHandler struct {
	service *service.CustomerService
}

func NewCustomerHandler(service *service.CustomerService) *CustomerHandler {
	return &CustomerHandler{service: service}
}

// GetCustomers
//@Summary      List customers
// @Description  get customers
// @Tags         customers
// @Accept       json
// @Produce      json
// @Param        sort query string false "sort"
// @Param        page query int false "page"
// @Param		 name query string false "name"
// @Param		 email query string false "email"
// @Param		 address.addressLine query string false "address line"
// @Param		 address.city query string false "city"
// @Param 		 address.country query string false "country"
// @Param		 address.cityCode query int false "city code"
// @Success      200  {object}  dto.SuccessResponseType
// @Failure		 404 {object} 	dto.ErrorType
// @Router       /api/customer 	[get]
func (h *CustomerHandler) GetCustomers(c echo.Context) error {
	customers := h.service.FindCustomers(c.QueryParams())
	return c.JSON(http.StatusOK, dto.NewSuccessResponse(customers))
}

// GetByIdCustomer
//@Summary       Show a customer
// @Description  get customer by ID
// @Tags         customers
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Customer ID"
// @Success      200  {object}  dto.SuccessResponseType
// @Failure		 404 {object} 	dto.ErrorType
// @Failure		 400 {object} 	dto.ErrorType
// @Router       /api/customer/{id} [get]
func (h *CustomerHandler) GetByIdCustomer(c echo.Context) error {
	customer := h.service.FindCustomer(c.Param("id"))
	return c.JSON(http.StatusOK, dto.NewSuccessResponse(customer))
}

// DeleteCustomer
//@Summary       Delete a customer
// @Description  delete customer by ID
// @Tags         customers
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Customer ID"
// @Success      200
//@Failure      400  {object}  dto.ErrorType
// @Failure		 404 {object} 	dto.ErrorType
// @Router       /api/customer/{id} [delete]
func (h *CustomerHandler) DeleteCustomer(c echo.Context) error {
	h.service.DeleteCustomer(c.Param("id"))
	return c.JSON(http.StatusNoContent, nil)
}

// CreateCustomer
//@Summary       Add a customer
// @Description  create customer
// @Tags         customers
// @Accept       json
// @Produce      json
// @Param 	dto.CustomerRequestType body dto.CustomerRequestType true "For create to Customer"
//@Success      200  {object}  dto.IdResponseType
// @Failure		 400 {object} 	dto.ErrorType
// @Failure		 500 {object} 	dto.ErrorType
// @Router       /api/customer [post]
func (h *CustomerHandler) CreateCustomer(c echo.Context) error {
	idResponse := h.service.CreateCustomer(c.Request().Body)
	return c.JSON(http.StatusCreated, idResponse)
}

// UpdateCustomer
//@Summary       Update a customer
// @Description  update customer
// @Tags         customers
// @Accept       json
// @Produce      json
// @Param        id   path      string  true  "Customer ID"
// @Param 		dto.CustomerRequestType body dto.CustomerRequestType true "For update to Customer"
// @Success      200  {object}  dto.QuickResponseType
// @Failure		 404 {object} 	dto.ErrorType
// @Failure		 400 {object} 	dto.ErrorType
// @Failure		 500 {object} 	dto.ErrorType
// @Router       /api/customer/{id} [put]
func (h *CustomerHandler) UpdateCustomer(c echo.Context) error {
	h.service.UpdateCustomer(c.Param("id"), c.Request().Body)
	return c.JSON(http.StatusOK, dto.QuickResponseType{Result: true})
}
