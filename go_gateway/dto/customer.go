package dto

type Address struct {
	AddressLine string `json:"addressLine" bson:"addressLine"`
	City        string `json:"city" bson:"city"`
	Country     string `json:"country" bson:"country"`
	CityCode    int    `json:"cityCode" bson:"cityCode"`
}

type CustomerResponseType struct {
	Name    string  `json:"name"`
	Email   string  `json:"email"`
	Address Address `json:"address"`
}

type CustomersResponseType struct {
	TotalItemCount int                    `json:"totalItemCount"`
	PageNumber     int                    `json:"pageNumber"`
	PageSize       int                    `json:"pageSize"`
	Data           []CustomerResponseType `json:"data"`
}

type CustomerRequestType struct {
	Name        string `json:"name"`
	Email       string `json:"email"`
	AddressLine string `json:"addressLine"`
	City        string `json:"city"`
	Country     string `json:"country"`
	CityCode    int    `json:"cityCode"`
}
