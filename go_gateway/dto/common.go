package dto

type QuickResponseType struct {
	Result bool `json:"result"`
}

type IdResponseType struct {
	Id interface{} `json:"id"`
}

type SuccessResponseType struct {
	IsSuccess bool        `json:"isSuccess"`
	Data      interface{} `json:"data"`
}

func NewSuccessResponse(data interface{}) SuccessResponseType {
	return SuccessResponseType{
		IsSuccess: true,
		Data:      data,
	}
}
