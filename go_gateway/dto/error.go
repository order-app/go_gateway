package dto

type ErrorType struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
