package dto

import "go.mongodb.org/mongo-driver/bson/primitive"

type OrderResponseType struct {
	CustomerId primitive.ObjectID `json:"customerId" bson:"customerId"`
	Quantity   int                `json:"quantity" bson:"quantity"`
	Price      float64            `json:"price" bson:"price"`
	Status     int                `json:"status" bson:"status"`
	Address    Address            `json:"address" bson:"address"`
	Product    ProductReqRes      `json:"product" bson:"product"`
}

type FindManyResponseType struct {
	TotalItemCount int                 `json:"totalItemCount"`
	PageNumber     int                 `json:"pageNumber"`
	PageSize       int                 `json:"pageSize"`
	Data           []OrderResponseType `json:"data"`
}

type ProductReqRes struct {
	ImageUrl string `json:"imageUrl" bson:"imageUrl"`
	Name     string `json:"name" bson:"name"`
}

type RequestOrderType struct {
	CustomerId primitive.ObjectID `json:"customerId" bson:"customerId"`
	Quantity   int                `json:"quantity" bson:"quantity"`
	Price      float64            `json:"price" bson:"price"`
	Address    Address            `json:"address" bson:"address"`
	Product    ProductReqRes      `json:"product" bson:"product"`
}

type ChangeStatusReq struct {
	Status string `json:"status" bson:"status"`
}
