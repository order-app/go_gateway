package cmd

import (
	"fmt"
	"github.com/labstack/echo/v4"
	echoSwagger "github.com/swaggo/echo-swagger"
	"order_customer_gateway/docs"
	"order_customer_gateway/go_gateway/client"
	"order_customer_gateway/go_gateway/controller"
	"order_customer_gateway/go_gateway/service"
	"order_customer_gateway/pkg/config"
	"order_customer_gateway/pkg/helper/middleware"
)

var (
	cfg       config.Properties
	clientCfg config.ClientProperties
)

func init() {
	cfg = config.EnvProperties["local"]
	clientCfg = config.EnvClientProperties["local"]
}

func Execute() {
	e := echo.New()
	e.Use(middleware.Recovery)
	e.GET("/swagger/*", echoSwagger.WrapHandler)
	docs.SwaggerInfo.Title = "Order-Customer_Gateway API"
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Description = "Order-Customer gateway"
	docs.SwaggerInfo.Host = fmt.Sprintf("%s:%s", cfg.Host, cfg.Port)
	docs.SwaggerInfo.Schemes = []string{"http"}
	customerClient := client.NewCustomerClient(clientCfg.CustomerBaseURI)
	serviceCustomer := service.NewCustomerService(customerClient)
	customerHandler := controller.NewCustomerHandler(serviceCustomer)
	clientOrder := client.NewOrderClient(clientCfg.OrderBaseURI)
	serviceOrder := service.NewOrderService(clientOrder)
	orderHandler := controller.NewOrderHandler(serviceOrder)
	CustomerRouter(e, customerHandler)
	OrderRouter(e, orderHandler)
	e.Start(fmt.Sprintf("%s:%s", cfg.Host, cfg.Port))
}

func CustomerRouter(e *echo.Echo, customerHandler *controller.CustomerHandler) {
	e.GET("/api/customer", customerHandler.GetCustomers)
	e.GET("/api/customer/:id", customerHandler.GetByIdCustomer)
	e.DELETE("/api/customer/:id", customerHandler.DeleteCustomer)
	e.POST("/api/customer", customerHandler.CreateCustomer)
	e.PUT("api/customer/:id", customerHandler.UpdateCustomer)
}

func OrderRouter(e *echo.Echo, orderHandler *controller.OrderHandler) {
	e.GET("/api/order", orderHandler.GetOrders)
	e.GET("/api/order/:id", orderHandler.GetByIdOrder)
	e.DELETE("/api/order/:id", orderHandler.DeleteOrder)
	e.POST("/api/order", orderHandler.CreateOrder)
	e.PUT("/api/order/:id", orderHandler.UpdateOrder)
	e.PUT("/api/order/status/:id", orderHandler.UpdateOrderStatus)
}
