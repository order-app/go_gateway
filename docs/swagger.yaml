definitions:
  dto.Address:
    properties:
      addressLine:
        type: string
      city:
        type: string
      cityCode:
        type: integer
      country:
        type: string
    type: object
  dto.ChangeStatusReq:
    properties:
      status:
        type: string
    type: object
  dto.CustomerRequestType:
    properties:
      addressLine:
        type: string
      city:
        type: string
      cityCode:
        type: integer
      country:
        type: string
      email:
        type: string
      name:
        type: string
    type: object
  dto.ErrorType:
    properties:
      code:
        type: integer
      message:
        type: string
    type: object
  dto.IdResponseType:
    properties:
      id: {}
    type: object
  dto.ProductReqRes:
    properties:
      imageUrl:
        type: string
      name:
        type: string
    type: object
  dto.QuickResponseType:
    properties:
      result:
        type: boolean
    type: object
  dto.RequestOrderType:
    properties:
      address:
        $ref: '#/definitions/dto.Address'
      customerId:
        type: string
      price:
        type: number
      product:
        $ref: '#/definitions/dto.ProductReqRes'
      quantity:
        type: integer
    type: object
  dto.SuccessResponseType:
    properties:
      data: {}
      isSuccess:
        type: boolean
    type: object
info:
  contact: {}
paths:
  /api/customer:
    get:
      consumes:
      - application/json
      description: get customers
      parameters:
      - description: sort
        in: query
        name: sort
        type: string
      - description: page
        in: query
        name: page
        type: integer
      - description: name
        in: query
        name: name
        type: string
      - description: email
        in: query
        name: email
        type: string
      - description: address line
        in: query
        name: address.addressLine
        type: string
      - description: city
        in: query
        name: address.city
        type: string
      - description: country
        in: query
        name: address.country
        type: string
      - description: city code
        in: query
        name: address.cityCode
        type: integer
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/dto.SuccessResponseType'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/dto.ErrorType'
      summary: List customers
      tags:
      - customers
    post:
      consumes:
      - application/json
      description: create customer
      parameters:
      - description: For create to Customer
        in: body
        name: dto.CustomerRequestType
        required: true
        schema:
          $ref: '#/definitions/dto.CustomerRequestType'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/dto.IdResponseType'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/dto.ErrorType'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/dto.ErrorType'
      summary: Add a customer
      tags:
      - customers
  /api/customer/{id}:
    delete:
      consumes:
      - application/json
      description: delete customer by ID
      parameters:
      - description: Customer ID
        in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: ""
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/dto.ErrorType'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/dto.ErrorType'
      summary: Delete a customer
      tags:
      - customers
    get:
      consumes:
      - application/json
      description: get customer by ID
      parameters:
      - description: Customer ID
        in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/dto.SuccessResponseType'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/dto.ErrorType'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/dto.ErrorType'
      summary: Show a customer
      tags:
      - customers
    put:
      consumes:
      - application/json
      description: update customer
      parameters:
      - description: Customer ID
        in: path
        name: id
        required: true
        type: string
      - description: For update to Customer
        in: body
        name: dto.CustomerRequestType
        required: true
        schema:
          $ref: '#/definitions/dto.CustomerRequestType'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/dto.QuickResponseType'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/dto.ErrorType'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/dto.ErrorType'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/dto.ErrorType'
      summary: Update a customer
      tags:
      - customers
  /api/order:
    get:
      consumes:
      - application/json
      description: get orders
      parameters:
      - description: sort
        in: query
        name: sort
        type: string
      - description: page
        in: query
        name: page
        type: integer
      - description: customerId
        in: query
        name: customerId
        type: string
      - description: quantity
        in: query
        name: quantity
        type: integer
      - description: price
        in: query
        name: price
        type: number
      - description: product name
        in: query
        name: product.name
        type: string
      - description: product.imageUrl
        in: query
        name: product.imageUrl
        type: string
      - description: address line
        in: query
        name: address.addressLine
        type: string
      - description: city
        in: query
        name: address.city
        type: string
      - description: country
        in: query
        name: address.country
        type: string
      - description: city code
        in: query
        name: address.cityCode
        type: integer
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/dto.SuccessResponseType'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/dto.ErrorType'
      summary: List orders
      tags:
      - orders
    post:
      consumes:
      - application/json
      description: create order
      parameters:
      - description: For create an Order
        in: body
        name: dto.RequestOrderType
        required: true
        schema:
          $ref: '#/definitions/dto.RequestOrderType'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/dto.IdResponseType'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/dto.ErrorType'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/dto.ErrorType'
      summary: Add an order
      tags:
      - orders
  /api/order/{id}:
    delete:
      consumes:
      - application/json
      description: delete order by ID
      parameters:
      - description: Order ID
        in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: ""
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/dto.ErrorType'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/dto.ErrorType'
      summary: Delete an order
      tags:
      - orders
    get:
      consumes:
      - application/json
      description: get order by ID
      parameters:
      - description: Order ID
        in: path
        name: id
        required: true
        type: string
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/dto.SuccessResponseType'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/dto.ErrorType'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/dto.ErrorType'
      summary: Show an order
      tags:
      - orders
    put:
      consumes:
      - application/json
      description: update order
      parameters:
      - description: Order ID
        in: path
        name: id
        required: true
        type: string
      - description: For update an Order
        in: body
        name: dto.RequestOrderType
        required: true
        schema:
          $ref: '#/definitions/dto.RequestOrderType'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/dto.QuickResponseType'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/dto.ErrorType'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/dto.ErrorType'
        "500":
          description: Internal Server Error
          schema:
            $ref: '#/definitions/dto.ErrorType'
      summary: Update an order
      tags:
      - orders
  /api/order/status/{id}:
    put:
      consumes:
      - application/json
      description: update order status
      parameters:
      - description: Order ID
        in: path
        name: id
        required: true
        type: string
      - description: For update an Order Status
        in: body
        name: dto.ChangeStatusReq
        required: true
        schema:
          $ref: '#/definitions/dto.ChangeStatusReq'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/dto.QuickResponseType'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/dto.ErrorType'
        "404":
          description: Not Found
          schema:
            $ref: '#/definitions/dto.ErrorType'
      summary: Update an order status
      tags:
      - orders
swagger: "2.0"
