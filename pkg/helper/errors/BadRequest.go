package errors

import (
	"net/http"
	_type "order_customer_gateway/pkg/helper/errors/type"
)

var(
	CustomerBindError = _type.ErrorType{
		Code:    http.StatusBadRequest,
		Message: "Unable to bind the request body.",
	}
)
