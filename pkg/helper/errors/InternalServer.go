package errors

import (
	"net/http"
	_type "order_customer_gateway/pkg/helper/errors/type"
)

var (
	ClientError = _type.ErrorType{
		Code:    http.StatusInternalServerError,
		Message: "HttpClient error.",
	}
)
