package middleware

import (
	"fmt"
	"github.com/labstack/echo/v4"
	_type "order_customer_gateway/pkg/helper/errors/type"
)

func Recovery(next echo.HandlerFunc) echo.HandlerFunc {
	return func (c echo.Context) error {
		defer func() {
			err := recover()
			if err != nil{
				resErr, cErr := err.(*_type.ErrorType)
				if cErr != true {
					fmt.Println(cErr)
				}
				c.JSON(resErr.Code, resErr)
			}
		}()
		return next(c)
	}
}
