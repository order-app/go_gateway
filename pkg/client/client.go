package client

import (
	"encoding/json"
	"github.com/labstack/gommon/log"
	"io"
	"net/http"
	"order_customer_gateway/pkg/helper/errors"
	_errType "order_customer_gateway/pkg/helper/errors/type"
	"time"
)

type Client struct {
	BaseURI    string
	HTTPClient *http.Client
}

func NewClient(url string) *Client {
	return &Client{
		BaseURI: url,
		HTTPClient: &http.Client{
			Timeout: time.Minute,
		},
	}
}

func (c *Client) SendRequest(req *http.Request) io.ReadCloser {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	res, doErr := c.HTTPClient.Do(req)
	if doErr != nil {
		log.Errorf("HttpClient Do error : %s", doErr)
		errors.Panic(errors.ClientError)
	}
	//defer res.Body.Close()
	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes *_errType.ErrorType
		if dErr := json.NewDecoder(res.Body).Decode(&errRes); dErr != nil {
			log.Errorf("Send request decode error message error : %s", dErr)
			errors.Panic(errors.ClientError)
		}
		errors.Panic(_errType.ErrorType{Code: errRes.Code, Message: errRes.Message})
	}
	return res.Body
}
