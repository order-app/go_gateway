package config

type Properties struct {
	Host string
	Port string
}

var EnvProperties = map[string]Properties{
	"local": {
		Host: "localhost",
		Port: "9000",
	},
}

type ClientProperties struct {
	CustomerBaseURI string
	OrderBaseURI    string
}

var EnvClientProperties = map[string]ClientProperties{
	"local": {
		CustomerBaseURI: "http://localhost:8080/customer",
		OrderBaseURI:    "http://localhost:8090/order",
	},
}
